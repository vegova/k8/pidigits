import java.awt.*;        // Using AWT container and component classes
import java.awt.event.*;  // Using AWT event classes and listener interfaces
 
// An AWT program inherits from the top-level container java.awt.Frame
public class testGUI extends Frame implements WindowListener { 
	private TextField tfInputUsername; 
	private TextField tfInputPassword;
	private Button btnLogin;
	private Button btnRegister;
	

	public testGUI () {
		setLayout(new FlowLayout());
 		
		tfInputUsername = new TextField("Login", 10);
		tfInputUsername.setEditable(true);
		add(tfInputUsername);
 		
		tfInputPassword = new TextField("Password", 10);
		tfInputPassword.setEditable(true);
		add(tfInputPassword);
 		

		btnLogin = new Button("Login");
		add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				System.out.println("Sending username and password");
			}
		});

		btnRegister = new Button("Register");
		add(btnRegister);
		btnRegister.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				System.out.println("Register");
			}
		});

		setTitle("PiDigits");
		setSize(450, 200);

		setVisible(true);         // "super" Frame shows
   }
 
   // The entry main() method
   public static void main(String[] args) {
      // Invoke the constructor to setup the GUI, by allocating an instance
      testGUI app = new testGUI();
         // or simply "new AWTCounter();" for an anonymous instance
   }

	@Override
	public void windowClosing(WindowEvent evt){
		System.exit(0);
	}
}