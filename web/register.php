<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Register</title>
  <link rel="stylesheet" type="text/css" href="css/register.css">
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  <div style="flex: 1 0 auto;">
    <div class="container">
    <br>
    <br>
    <br>
    <br>
    <!-- Login -->
    <div class="row">
        <div class="col offset-s3 s6">
          <h3> Register for PiDigits </h3>
          <input placeholder="username" type="text" id="username">
          <p class="alert hidden" id="usernameTakenAlert"> This username is already taken</p>
          
          <input placeholder="email" type="email" id="email">
          <p class="alert hidden" id="accountWithEmailAlreadyExistsAlert"> Account with this username already exists</p>
          
          <input placeholder="password" type="password" id="password">
          <p class="alert hidden" id="passwordTooShortAlert"> Password should be at least 6 characters long</p>

          <button id="registerBtn" class="btn" style="background-color: rgb(204, 0, 51)">Register</button>
          <p> Already have an account? <a href="login.php" id="under"> Log in here</a></p>
        </div>
      </div>
    </div>
  </div>
  
  <?php include("frames/footer.php"); ?>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/script.js"></script>
  <script src="js/register.js"></script>

  </body>
</html>