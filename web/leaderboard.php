<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Leaderboard</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

  <?php ini_set('display_errors', 1); include("frames/navigation.php"); ?>


  <div style="flex: 1 0 auto;">

    <h1 style="text-align: center; color: rgb(241, 0, 51)">Leaderboard</h1>

    <table style="position: relative; left: 10%; right: 10%; width: 80%;">
      <tbody id="leaderboard">
        <tr>
          <th style="width: 20%;">Place</th>
          <th style="width: 50%;">Username</th>
          <th style="width: 30%;">Score</th>
        </tr>
        <?php 
            
            $getUsers = $pdo->prepare("SELECT `Username`, `CalculatedDigits` FROM `users` WHERE `Username` != 'guest' ORDER BY `CalculatedDigits` DESC LIMIT 20");
            $getUsers->execute();
            $users = $getUsers->fetchAll();

            $i = 1;
            foreach($users as $user){

                echo "<tr>";
                echo "<td>" . $i ."</td>";
                echo "<td>" . $user["Username"] ."</td>";
                echo "<td>" . $user["CalculatedDigits"] ."</td>";
                echo "</tr>";

                $i++;
            }

        ?>
      </tbody>
    </table>
     <br>
     <br>
     <br>
  </div>

  <?php include("frames/footer.php"); ?>

  <script src="js/script.js"></script>

</body>