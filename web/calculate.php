<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <title>Calculate</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  <br>
  <br>

  <div style="flex: 1 0 auto;">
    <div class="container">
      <div class="row">
        <div class="col offset-s4 s4">
          <h3> Calculating Pi... </h3>
          <br>
        </div>
        <div class="col offset-s3 s6">
          <h5> In this session, you have calculated <span id="digitCount"> 0 </span> digits of Pi. </h5>
          <h5> In total, you have calculated <span id="totalDigitCount"> 
          
          <?php 
          include "core/dbConnect.php";
          ini_set('display_errors', 1);

          if(isset($_COOKIE["uid"])){
            // uid is set
            $uid = $_COOKIE["uid"];

            $getNumberOfCalculated = "SELECT `CalculatedDigits` FROM `users` WHERE `UID`='$uid'";
            $numberOfCalculated = $conn->query($getNumberOfCalculated)->fetch_assoc();

            echo $numberOfCalculated["CalculatedDigits"];
          }
          else{
            echo 0;
          }

          $conn->close();

          ?> </span> digits of Pi. </h5>
          
          <h4 class="center">Thank you! </h4>

          <p class="center"> Average calculation speed: <span id="dps"> 0 </span> digits per second </p>
        </div>
      </div>  
    </div>
    <p class="center"> Want to calculate faster? Open more tabs! Pi Digits will work with as many tabs as you like. Just make sure you're logged in so the score is attributed to your account.</p>
  </div>

  <?php include("frames/footer.php"); ?>
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/script.js"></script>
  <script src="js/calculate.js"></script>

  <script>
    var w;
    var digitCounter = 0;
    var user = (getUser() == null) ? false : getUser();
    var block = "";
    var startDate = new Date();;
    var endDate;

    function startWorker() {
      if (typeof(Worker) !== "undefined") {
        if (typeof(w) == "undefined") {
          w = new Worker("js/calculate.js");

          prepareForCalc();
        }
      } else {
        console.log("Sorry! No Web Worker support.");
      }
    }
    startWorker();

    function prepareForCalc(){
      
      postAjax('core/getDigits.php', false, function(data){
        data = parseInt(data);
        w.postMessage(data)
        
        w.onmessage = function(e) {
          block = block + "" + e.data;
          console.log(e.data)
          console.log(block.length)
          
          digitCounter+=5;
          document.getElementById("digitCount").innerText = digitCounter;

          var endDate   = new Date();
          var seconds = (endDate.getTime() - startDate.getTime()) / 1000;

          var dps = Math.round(digitCounter / seconds * 100) / 100;
          console.log("dps: " + dps);

          document.getElementById("dps").innerText = dps;

          var totalDigits = parseInt(document.getElementById("totalDigitCount").innerText);

          document.getElementById("totalDigitCount").innerText = totalDigits + 5;
          
          if(block.length >= 50){
            var digits = block.substring(0,50);
              postAjax('core/send.php', { uid: user.uid, startDigitIndex: data, digits: digits}, function(data){
                block =  "";
                prepareForCalc();
              });
          }
        }
      });
    }

    function stopWorker() {
      w.terminate();
      w = undefined;
    }
  </script>
  
  </body>
</html>