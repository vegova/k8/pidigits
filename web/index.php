<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="stylesheet" type="text/css" href="css/index.css">
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Pi Digits</title>
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  <div style="flex: 1 0 auto;">
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center white-text"><span id="pi">π </span><span id="red">Digits<span></h1>
        <div class="row center">
          <h5 class="header col s12 light">Distributed calculation of pi</h5>
        </div>
        <div class="row center">
          <a href="calculate.php" class="btn-large" id="redBack" style="background-color: rgb(204, 0, 51)">Donate CPU time</a>
        </div>
      <br><br>
    </div>
  </div>
</div>

  <?php include("frames/footer.php"); ?>
  <script src="js/script.js"></script>

  </body>
</html>
