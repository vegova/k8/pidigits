<?php

include 'connect.php';
ini_set('display_errors', 1);
$username = $_POST["username"];
$uid = $_POST["uid"];

$getUsers = $pdo->prepare("SELECT `UID` FROM `users` WHERE `username`=:username");
$getUsers->execute([":username" => $username]);
$users = $getUsers->fetch();

$getUsers = null;

if(!$users){
    $sqlUpdate = $pdo->prepare("UPDATE `users` SET `Username`=:username WHERE `UID`=:uid");
    $sqlUpdate->execute([":username" => $username, ":uid" => $uid]);
    $sqlUpdate = null;
    echo json_encode(true);
    
}
else{
    echo json_encode(false);
}
