<?php

// TODO: Dodaj podatke za podatkovno bazo
$username = "";
$password = "";
$db_name = "";

$dsn = "mysql:host=localhost;dbname=".db_name.";charset=utf8mb4";
$options = [
    PDO::ATTR_EMULATE_PREPARES   => false, // Turn off emulation mode for "real" prepared statements
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, // Turn on errors in the form of exceptions
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, // Make the default fetch be an associative array
];
try {
    $pdo = new PDO($dsn, $username, $password, $options);
} catch (Exception $e) {
    echo('Error connecting to the database');
}