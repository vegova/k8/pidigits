<?php
ini_set('display_errors', 1);
require "connect.php";

const blockLength = 1024;
const calculationBlock = 50;

$getFirstValidDigit = $pdo->prepare("SELECT `DigitIndex` FROM `queue` WHERE `State`= '0' ORDER BY `DigitIndex` ASC LIMIT 1");
$getFirstValidDigit->execute();
$firstValidDigit = $getFirstValidDigit->fetch();
echo $firstValidDigit["DigitIndex"]*calculationBlock;



// QUEUE STATES
// 0 - Not being calculated
// 1 - Currenty being calculated by someone
// 2 - Already calculated

// Set state of the selected index to 1
$updateRow = $pdo->prepare("UPDATE `queue` SET `State`= 1 WHERE `DigitIndex`=:firstValidDigit");
$updateRow->execute([":firstValidDigit" => $firstValidDigit["DigitIndex"]]);

// Add a new digit to queue
$insertToQueue = $pdo->prepare("INSERT INTO `queue`() VALUES ()");
$insertToQueue->execute();
