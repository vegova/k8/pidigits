<?php

require "connect.php";


const blockLength = 1024;
const calculationBlock = 50;

// Select already calculated rows in the queue
$selectCalculated = $pdo->prepare("SELECT * FROM `queue` WHERE `State`= '2' ORDER BY `DigitIndex`");
$selectCalculated->execute();
$calculated = $selectCalculated->fetchAll();

foreach($calculated as $calculatedRow){

  // Get the start index of the last calculated digits
  $selectLastRowInDB = $pdo->prepare("SELECT Digits, MAX(DigitBlock) AS DigitBlock FROM pi GROUP BY DigitBlock DESC");
  $selectLastRowInDB->execute();
  $lastRowInDB = $selectLastRowInDB->fetch();

  $dbIndex = (($lastRowInDB["DigitBlock"] - 1) * blockLength + strlen($lastRowInDB["Digits"]));

  $queueIndex = $calculatedRow["DigitIndex"] * calculationBlock;

  echo ("\n" .$queueIndex . " " . $dbIndex);

  if($queueIndex === $dbIndex){

    // Move the value from the queue to the db and then remove the row from the queue
    moveDigitsToDb($calculatedRow["Digits"], $conn);

    $removeIndex = $calculatedRow["DigitIndex"];

    $removeFromQueue = $pdo->prepare("DELETE FROM `queue` WHERE `DigitIndex`=:removeIndex");
    $removeFromQueue->execute([":removeIndex" => $removeIndex]);
  }
}



// Reset old calculation orders
$resetOldOrders = $pdo->prepare("UPDATE `queue` SET `State` = 0 WHERE `CreatedOn` < (NOW() - INTERVAL 10 MINUTE) AND `CreatedOn` != '0000-00-00 00:00:00' AND `State`= 1;");
$resetOldOrders->execute();

function moveDigitsToDb($digits) {
  global $pdo;

  $getLastBlock = $pdo->prepare("SELECT Digits, MAX(DigitBlock) AS DigitBlock FROM pi GROUP BY DigitBlock DESC");
  $getLastBlock->execute();
  $lastBlock = $getLastBlock->fetch();

  $oldDigits = $lastBlock["Digits"];
  
  $oldBlockDigits = substr($digits, 0, blockLength - strlen($oldDigits)); // Digits that need to be appended to the current block 
  $newBlockDigits = substr($digits, blockLength - strlen($oldDigits)); // Digits that need to be added to a new block
  
  $oldBlockDigits = $oldDigits . $oldBlockDigits;

  $digitBlock = $lastBlock["DigitBlock"];

  $updateCurrentBlock = $pdo->prepare("UPDATE `pi` SET `Digits`=:oldBlockDigits WHERE `DigitBlock`=:digitBlock");
  $updateCurrentBlock->execute([":oldBlockDigits" => $oldBlockDigits, ":digitBlock" => $digitBlock]);


  // If necessary, add a new block 
  if($newBlockDigits){
    $newBlock = $pdo->prepare("INSERT INTO `pi`(`Digits`) VALUES (:newBlockDigits)");
    $newBlock->execute([":newBlockDigits" => $newBlockDigit]);
  }
}