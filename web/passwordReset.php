<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Password Reset</title>
  <link rel="stylesheet" type="text/css" href="css/passwordReset.css">
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  <div style="flex: 1 0 auto;">
    <div class="container">
      <div class="row">
        <div class="col offset-s4 s4">
          <h3> Forgot password </h3>
          <input placeholder="email" type="email" id="email">
          <p class="alert hidden" id="noEmailAlert"> Write an email address</p>
          <button id="sendEmailBtn" class="btn" style="background-color: rgb(204, 0, 51)">Send reset link</button>
          <p id="error"></p>
        </div>
      </div>
      </div>
    </div>
  </div>
  
  <?php include("frames/footer.php"); ?>

  <script src="js/passwordReset.js"></script>

  </body>
</html>