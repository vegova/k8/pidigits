<?php 
    require_once "core/connect.php";
    
    if(isset($_COOKIE["uid"])){
        $isUserAdmin = $pdo->prepare("SELECT `Username` FROM `users` WHERE `UID`=:uid AND `AccountType`=1");
        $isUserAdmin->execute(["uid" => $_COOKIE["uid"]]);
        
        $isAdmin = $isUserAdmin->fetch();

        if(!$isAdmin){
            header("Location: index.php", 301);
            exit();
        };

        $isUserAdmin = null;
    }
    else{
        header("Location: index.php", 301);
        exit();
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Users</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

  <?php include("frames/navigation.php"); ?>


  <div style="flex: 1 0 auto;">

    <h1 style="text-align: center; color: rgb(241, 0, 51)">Users</h1>

    <table style="position: relative; left: 10%; right: 10%; width: 80%;">
      <tbody id="leaderboard">
        <tr>
          <th style="width: 40%;">Username</th>
          <th style="width: 30%;">Score</th>
          <th style="width: 30%;">Account type</th>
        </tr>
        <?php 
            
            $getUsers = $pdo->prepare("SELECT * FROM `users`");
            $getUsers->execute();
            $users = $getUsers->fetchAll();

            foreach($users as $user){
                echo "<tr>";
                echo "<td>" . $user["Username"] ."</td>";
                echo "<td>" . $user["CalculatedDigits"] ."</td>";
                echo "<td>" . accountType($user)."</td>";
                echo "</tr>";
            }

            function accountType($user){
                return ($user["AccountType"] == 1) ? "Admin" : "User";
            }
        ?>
      </tbody>
    </table>

    <br>
     
  </div>

  <?php include("frames/footer.php"); ?>

  <script src="js/script.js"></script>

</body>