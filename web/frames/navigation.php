<nav style="background-color: rgb(204,0,51)">
    <div class="nav-wrapper container"> 
  
    <ul class="left">
      <li id="vegovaLogo"> <img src="images/vegovaVodoravna.svg" style="height: 60px"></li>
      <li id="piLogo"> <span id="homeBtn">π</span></li>
    </ul>
      
    <ul class="right">
      <?php 
        require_once "core/connect.php";

        if(isset($_COOKIE["uid"])){
          $isUserAdmin = $pdo->prepare("SELECT `Username` FROM `users` WHERE `UID`=:uid AND `AccountType`=1");
          $isUserAdmin->execute(["uid" => $_COOKIE["uid"]]);
          
          $isAdmin = $isUserAdmin->fetch();
          if(!$isAdmin) exit();

          echo "<li> <a href='users.php'>Users</a></li>";
          
        }
      ?>
      <li> <a href="leaderboard.php">Leaderboard</a></li>
      <li><a class="hidden" id="profileDisplay" href="profile.php">Profile</a></li>
      <li><a id="loginDisplay" href="login.php">Login</a></li>
    </ul>
    </div>
  </nav>
