<footer class="page-footer" style="flex-shrink: 0;">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h4 class="white-text">About us</h4>
        <p class="grey-text text-lighten-4">
          We are a team of students from Vegova Ljubljana with a goal of creating a 
          platform for distributed calculation of Pi as precisely as possible.
        </p>


      </div>
      <div class="col l3 s12">
        <h4 class="white-text">Settings</h4>
        <ul>
          <li><a class="white-text" href="login.php">Login</a></li>
          <li><a class="white-text" href="register.php">Sign Up</a></li>
          <li><a class="white-text" href="profile.php">My profile</a></li>
        </ul>
      </div>
      <div class="col l3 s12">
        <h4 class="white-text">Connect</h4>
        <ul>
          <li><a class="white-text" href="https://www.reddit.com/r/pidigits">Facebook</a></li>
          <li><a class="white-text" href="https://www.reddit.com/r/pidigits">Twitter</a></li>
          <li><a class="white-text" href="https://www.reddit.com/r/pidigits">Reddit</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    Made with <a class="red-text text-lighten-3" href="http://materializecss.com" target="_blank">Materialize</a>
    </div>
  </div>
</footer>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.6.0/firebase.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>