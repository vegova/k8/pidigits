﻿<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
  <link rel="icon" href="images/pi.png" type="image/bmp">
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="css/login.css">
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  
  <div style="flex: 1 0 auto;">
    <div class="container">
      <br>
      <br>
      <br>
      <br>
      <!-- Login -->
      <div class="row">
        <div class="col offset-s3 s6">
            <h3> Login to PiDigits </h3>
            <input placeholder="email" type="email" id="email" style="text-color: rgb(150, 150, 150)">
            <p class="alert hidden" id="incorrectEmailAlert"> user with this email does not exist</p>

            <input class="two" placeholder="password" type="password" id="password">
            <p class="alert hidden" id="incorrectPasswordAlert"> Incorrect password</p>

            <button id="loginBtn" class="btn" style="background-color: rgb(204, 0, 51)">Log In</button>
            <p><a href="passwordReset.php" id="under">Forgot password? </a></p>
            <p> Don't have an account? <a href="register.php" id="under"> Create an account here</a></p>
        </div>
      </div>
    </div>
  </div>
  
  <?php include("frames/footer.php"); ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="js/script.js"></script>
  <script src="js/login.js"></script>

  </body>
</html>