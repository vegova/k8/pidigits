<!DOCTYPE html>
<html lang="en">
<head>
<?php include("frames/head.php"); ?>
<link rel="icon" href="images/pi.png" type="image/bmp">
  <title>My Profile</title>
  <link rel="stylesheet" type="text/css" href="css/profile.css">
</head>

<body>
  
  <?php include("frames/navigation.php"); ?>

  <div style="flex: 1 0 auto;">
    <div class="row">
      <div class="col s6 offset-s3">
        <div class="card darken-1">
          <div class="card-content">
            <span class="card-title">Account</span>

            <br>
            <span> Username </span>
            <input placeholder="Username" id="newUsername"> 
            <p class="alert hidden" id="usernameTakenAlert"> This username is already taken. Please choose a different one.</p>
            
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col s6 offset-s3">
        <div class="card darken-1">
          <div class="card-content">
            <span class="card-title">Security</span>
            <p> For security reasons, you need to reenter your current password before making changes in this area </p>

            <br>
            <span> Current Password</span>
            <input type="password" placeholder="Current password" id="currentPassword">
            <p class="alert hidden" id="currentPasswordAlert"> Current password is required for this operation</p>
            <p class="alert hidden" id="incorrectPasswordAlert"> Incorrect password</p>

            <br>
            <br>
            <span> Email</span>
            <input placeholder="Email" id="email">  
            <p class="confirmEmail hidden" id="sendEmailConfirmation"> This email has not been confirmed yet. Click here to send a confirmation email</p>   

            <br>
            <br>
            <span> Password</span>
            <input type="password" placeholder="Password" id="password"> 
            <input type="password" placeholder="Reenter password" id="reenterPassword"> 
            <p class="alert hidden" id="passwordsDontMatchAlert"> Passwords don't match</p>
            <p class="alert hidden" id="passwordTooShortAlert"> New password should be at least 6 characters long</p>
            
            <button id="deleteAccount" class="btn" style="background-color: rgb(204, 0, 51)"> Delete account</button>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col s6 offset-s3">
        <div class="card darken-1">
          <div class="card-content">
            <span class="card-title">Actions</span>

            <button id="saveBtn" class="btn" style="background-color: rgb(204, 0, 51)"> Save </button>
            <button id="logoutBtn" class="btn" style="background-color: rgb(204, 0, 51)"> Log Out </button>


            <p class="alert hidden" id="error"> </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <?php include("frames/footer.php"); ?>
  <script src="js/script.js"></script>
  <script src="js/profile.js"></script>

  </body>
</html>