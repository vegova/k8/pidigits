// Initialize Firebase
var config = {
  // TODO: Dodaj firebase config tukaj
};

firebase.initializeApp(config);

document.getElementById("sendEmailBtn").addEventListener("click", function(){

  var email = document.getElementById("email").value;
  
  var auth = firebase.auth();

  document.getElementById("noEmailAlert").classList.add("hidden");

  if(email === ""){
    document.getElementById("noEmailAlert").classList.remove("hidden");
  }
  else{
    auth.sendPasswordResetEmail(email).then(function() {
      M.toast({html: 'Email sent successfully! Redirecting to login.'})
      window.location = "login.php"
    }).catch(function(error) {
      // An error happened.
    });
  }  
})