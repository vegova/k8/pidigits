// Initialize Firebase
var config = {
    // TODO: Dodaj firebase config tukaj
  };
  firebase.initializeApp(config);
  


// On document load
window.addEventListener('DOMContentLoaded', function () {

    // Check whether the user is logged in
    var user = getUser();
    if (user !== null) {
        // User is logged in

        document.getElementById("loginDisplay").innerHTML = "Logout";
        document.getElementById("loginDisplay").href = "profile.php"
    } else {
        // User is not logged in

        document.getElementById("loginDisplay").innerHTML = "Login";
        document.getElementById("loginDisplay").href = "login.php"
    }
});


function getUser() {
    return JSON.parse(localStorage.getItem("user"));
}

// Logout
document.getElementById("loginDisplay").addEventListener("click", function(){
    if(document.getElementById("loginDisplay").innerHTML == "Logout"){
        console.log("Logging out ...")
  
        firebase.auth().signOut().then(function() {
          M.toast({html: 'Logged out successfully!'})
          localStorage.removeItem("user");
          eraseCookie("uid");
        }).catch(function(error) {
          console.log("An error occured" + error);
        });
    }
  })

  
function postAjax(url, data, success) {
  var params = typeof data == 'string' ? data : Object.keys(data).map(
      function (k) {
          return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
      }
  ).join('&');

  var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
  xhr.open('POST', url);
  xhr.onreadystatechange = function () {
      if (xhr.readyState > 3 && xhr.status == 200) {
          success(xhr.responseText);
      }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.send(params);

}

document.getElementById("vegovaLogo").addEventListener("click", function(){
    var win = window.open("https://www.vegova.si/", '_blank');
    win.focus();
})

document.getElementById("piLogo").addEventListener("click", function(){
    window.location = "index.php";
})

function setCookie(name,value,days) {
    let expires = "";
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    const nameEQ = name + "=";
    const ca = document.cookie.split(';');
    for(let i=0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}