
// On register button click
document.getElementById("registerBtn").addEventListener("click", function(){
    var username = document.getElementById("username").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;

    document.getElementById("usernameTakenAlert").classList.add("hidden");
    document.getElementById("passwordTooShortAlert").classList.add("hidden");
    document.getElementById("accountWithEmailAlreadyExistsAlert").classList.add("hidden");


    if(password.length < 6){
        document.getElementById("passwordTooShortAlert").classList.remove("hidden");
    }
    else{
        postAjax('core/usernameAvailability.php', { username: username}, function(data){
            if(JSON.parse(data) == true){

                firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {

                    var errorCode = error.code;
                    var errorMessage = error.message;

                    switch(error.code){
                        case "auth/email-already-in-use":
                            document.getElementById("accountWithEmailAlreadyExistsAlert").classList.remove("hidden");
                            break;
                    }
                })

            }
            else{
                document.getElementById("usernameTakenAlert").classList.remove("hidden");
            }
        });
    }
})

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {

        // Set the user's username
        user.updateProfile({
            displayName: document.getElementById("username").value,
        }).then(function() {

            postAjax('core/createUsername.php', { username: user.displayName, uid: user.uid}, function(data){
                if(JSON.parse(data) == true){
                    localStorage.setItem("user", JSON.stringify(user));
                    window.location = "profile.php";
                }
                else{
                    document.getElementById("error").innerHTML = result;
                }
            });
        }).catch(function(error) {
            console.log("An error occurred" + error)
        });

    }
});
