onmessage = function(e) {
	var block = "";
	data = parseInt(e.data);
		
	while(block.length < 50){	
		//console.log("index: " + data) 
		
		var pi = calc(parseInt(data));
		postMessage(pi);
		data+=5;
	}
	
}

function calc(d){

	//vars
	var sum1 = sumJ(d,1.0); //{16**d * S1}
	var sum4 = sumJ(d,4.0); //{16**d * S4}
	var sum5 = sumJ(d,5.0); //{16**d * S5}
	var sum6 = sumJ(d,6.0); //{16**d * S6}

	var sum0 = 4.0*sum1 - 2.0*sum4 - sum5 - sum6;

	sum0 -= parseInt(sum0, 10); //porezemo celi del {16**d * pi}
	//sum0 spremenimo v 5 hex decimalk, zaradi natancnosti
	//prverimo ce je sum0 < 0 ker ce je moramo hex decimalke vsako posebej negirati
	if (sum0 > 0){
		//ponavljamo dokler ne ekstratktamo 5 decimalk
		var DEC="";
		for (var i=0;i<5 ;i++ ) {
			var a = 16.0*sum0; //pomnožimo da dobimo hex
			DEC += parseInt(a, 10).toString(16); //naredimo hex obliko decimalke
			sum0 = a - parseInt(a);
    }
		return DEC;
	} else{
		var DEC2="";
		for (var i=0;i<5 ;i++ ) {
			var a = 16.0*sum0; //pomnožimo da dobimo hex
			var DEC = tilde(Math.abs(parseInt(a,10))); //naredimo hex obliko decimalke
			DEC2 += DEC[7];
			sum0 = a - parseInt(a, 10);
		}
		return DEC2;
	}
	//racuna {16**d Sj}
	function sumJ(d, j){
		var sum1 = 0.0;
		var sum2 = 0.0;

		for (var k=0.0; k < (d+1); k++) {
			sum1 += (modulo(16, parseInt(d-k, 10), parseInt(8*k+j, 10))) / (8.0*k + j);
		}
		sum1-= parseInt(sum1, 10);

		var k = d + 1;

		for (var i = 0; i < 100 ; i++) {
			sum2 += (Math.pow(16.0, d - k) / (8.0*k + j));
			k++;
		}
		return (sum1+sum2 - parseInt(sum1+sum2, 10));
	}

	//moj ultimate modulo
	function modulo(a, b, c){
		var x=1;
		var y=a;
		while(b > 0){
			if(b%2 == 1){
				x=(x*y)%c;
			}
			y = (y*y)%c; // squaring the base
			b /= 2;
			b = parseInt(b, 10);
		}
		return parseInt(x%c, 10);
	}

	function tilde(a){
		a = a.toString(2);
		for(var i=0, c=a.length; i<(32-c);i++) a = "0" + a;
		for(var i=0; i<a.length; i++) a=(a[i]=='0')?(a.substr(0, i) + '1' + a.substr(i+1)):(a.substr(0, i) + '0' + a.substr(i+1));
		a = parseInt(a,2);
		return a.toString(16);
	}
}
