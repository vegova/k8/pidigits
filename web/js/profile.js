var user;

firebase.auth().onAuthStateChanged(function(u) {
  if (u) {
    user = u;
    // User is logged in
    populateFields();
  } else {
    // User is not logged in, redirect to the login page
    window.location="login.php";
  }
});

function populateFields(){
  document.getElementById("newUsername").value = user.displayName;
  document.getElementById("email").value = user.email;

  if(!user.emailVerified){
    document.getElementById("sendEmailConfirmation").classList.remove("hidden");
  }
}


// Delete account
document.getElementById("deleteAccount").addEventListener("click", function(){

  var currentPassword = document.getElementById("currentPassword").value;

  document.getElementById("currentPasswordAlert").classList.add("hidden");
  document.getElementById("incorrectPasswordAlert").classList.add("hidden");

  var credential = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);

  if(currentPassword === ""){
    document.getElementById("currentPasswordAlert").classList.remove("hidden");
  }
  else{

    user.reauthenticateAndRetrieveDataWithCredential(credential).then(function() {

      postAjax('core/deleteUser.php', {uid: user.uid}, function(data){
        if(JSON.parse(data) == true){
          
          user.delete().then(function() {
            M.toast({html: 'Account deleted successfully!'})
            localStorage.removeItem("user");
            eraseCookie("uid");
          }).catch(function(error) {
            console.log("An error occured" + error);
          });

        }
        else{
            console.log("Error deleting user");
        }
      });

    }).catch(function(error) {
      switch (error.code) {
          case "auth/wrong-password":
              document.getElementById("incorrectPasswordAlert").classList.remove("hidden");
              break;
      }
    });
  }
})


// Save settings
document.getElementById("saveBtn").addEventListener("click", function(){
  var username = document.getElementById("newUsername").value;
  var email = document.getElementById("email").value;
  var currentPassword = document.getElementById("currentPassword").value;
  var newPassword = document.getElementById("password").value;
  var newPasswordConfirm = document.getElementById("reenterPassword").value;

  var credential = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);

  // Clear alerts
  document.getElementById("usernameTakenAlert").classList.add("hidden");
  document.getElementById("currentPasswordAlert").classList.add("hidden");
  document.getElementById("incorrectPasswordAlert").classList.add("hidden");
  document.getElementById("passwordTooShortAlert").classList.add("hidden");
  

  if(username != user.displayName){
    // Change username
    changeUsername();
  }

  if(email != user.email){
    if(currentPassword === ""){
      document.getElementById("currentPasswordAlert").classList.remove("hidden");
    }
    else{

      // Change email
      user.reauthenticateAndRetrieveDataWithCredential(credential).then(function() {
    
        user.updateEmail(email).then(function() {
          M.toast({html: 'Email changed successfully!'})
          document.getElementById("currentPassword").value = "";

          populateFields(); 
        }).catch(function(error) {
          console.log(error);
        }); 

    
      }).catch(function(error) {
        switch (error.code) {
            case "auth/wrong-password":
                document.getElementById("incorrectPasswordAlert").classList.remove("hidden");
                break;
        }
      });
    } 
  }
  if(newPassword !== ""){
    if(newPassword !== newPasswordConfirm){
      document.getElementById("passwordsDontMatchAlert").classList.remove("hidden");
    }
    else if(currentPassword === ""){
      document.getElementById("currentPasswordAlert").classList.remove("hidden");
    }
    else if(newPassword.length < 6){
      document.getElementById("passwordTooShortAlert").classList.remove("hidden");
    }
    else{
      user.reauthenticateAndRetrieveDataWithCredential(credential).then(function() {

        user.updatePassword(newPassword).then(function() {
          M.toast({html: 'Password changed successfully!'})

          document.getElementById("password").value = "";
          document.getElementById("currentPassword").value = "";
          document.getElementById("reenterPassword").value = "";

        }).catch(function(error) {
          console.log(error);
        });

      }).catch(function(error) {
        switch(error){
          case "auth/wrong-password":
              document.getElementById("incorrectPasswordAlert").classList.remove("hidden");
              break;
        }
      });
    }
  }
})

function changeUsername(){
  var username = document.getElementById("newUsername").value;
  var data = {
    "username": username,
    "uid": user.uid,
  }

  postAjax('core/updateUsername.php', { username: username, uid: user.uid}, function(data){
    if(JSON.parse(data) == true){
        user.updateProfile({
            displayName: username,
        }).then(function() {
            M.toast({html: 'Username changed successfully!'})

            populateFields();

        }).catch(function(error) {
            console.log("Username change failed");
            console.log(error);
        });
    }
    else{
        console.log("Error changing username");
        document.getElementById("usernameTakenAlert").classList.remove("hidden");
    }
  });
}


// Logout
document.getElementById("logoutBtn").addEventListener("click", function(){
  firebase.auth().signOut().then(function() {
    M.toast({html: 'Logged out successfully!'})
    localStorage.removeItem("user");
    eraseCookie("uid");
  }).catch(function(error) {
    console.log("An error occured" + error);
  });
})

document.getElementById("sendEmailConfirmation").addEventListener("click", function(){

  user.sendEmailVerification().then(function() {
    M.toast({html: 'Email verification sent successfully!', displayLength: 1000})
  }).catch(function(error) {
    console.log("An error occured" + error);
  });
})


