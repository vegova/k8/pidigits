
// On login button click 
document.getElementById("loginBtn").addEventListener("click", function(){
  document.getElementById("incorrectEmailAlert").classList.add("hidden");
  document.getElementById("incorrectPasswordAlert").classList.add("hidden");

  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;

  firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    
    switch(error.code){
      case "auth/user-not-found":
        document.getElementById("incorrectEmailAlert").classList.remove("hidden");
        break;
      case "auth/wrong-password":
        document.getElementById("incorrectPasswordAlert").classList.remove("hidden");
        break;
    }
  });
})

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {

    localStorage.setItem("user", JSON.stringify(user));
    setCookie("uid",user.uid,265);
    window.location = "profile.php";

  } else {
    // No user is signed in.
  }
});
