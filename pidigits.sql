-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Gostitelj: 127.0.0.1
-- Čas nastanka: 24. jan 2019 ob 09.40
-- Različica strežnika: 10.1.34-MariaDB
-- Različica PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Zbirka podatkov: `pidigits`
--

-- --------------------------------------------------------

--
-- Struktura tabele `pi`
--

CREATE TABLE `pi` (
  `DigitBlock` int(11) NOT NULL,
  `Digits` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `pi`
--

INSERT INTO `pi` (`DigitBlock`, `Digits`) VALUES
(1, '243F6A8885A308D313198A2E03707344A4093822299F31D008');

-- --------------------------------------------------------

--
-- Struktura tabele `queue`
--

CREATE TABLE `queue` (
  `DigitIndex` int(11) NOT NULL,
  `State` tinyint(11) NOT NULL DEFAULT '0',
  `Digits` text NOT NULL,
  `CreatedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabele `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `UID` text NOT NULL,
  `Username` text NOT NULL,
  `CalculatedDigits` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Odloži podatke za tabelo `users`
--

INSERT INTO `users` (`Id`, `UID`, `Username`, `CalculatedDigits`) VALUES
(1, 'Guest', 'Guest', 0),
(3, 'G3fGspLlGRceMEovDpWbixDMZpH3', 'Urboom', 0),
(4, 'NhVwfudKTWU3uads8vXpJlDcub93', 'tomaz', 0),
(6, '8lCaBn8hI6U72ZKDECZ5lMn5IZw2', 'Vid', 50);

--
-- Indeksi zavrženih tabel
--

--
-- Indeksi tabele `pi`
--
ALTER TABLE `pi`
  ADD PRIMARY KEY (`DigitBlock`);

--
-- Indeksi tabele `queue`
--
ALTER TABLE `queue`
  ADD PRIMARY KEY (`DigitIndex`);

--
-- Indeksi tabele `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT zavrženih tabel
--

--
-- AUTO_INCREMENT tabele `pi`
--
ALTER TABLE `pi`
  MODIFY `DigitBlock` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT tabele `queue`
--
ALTER TABLE `queue`
  MODIFY `DigitIndex` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT tabele `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
